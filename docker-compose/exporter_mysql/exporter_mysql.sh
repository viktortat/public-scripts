#!/bin/bash
## curl https://gitlab.com/itc-life/public-scripts/raw/master/docker-compose/exporter_mysql/exporter_mysql.sh -o exporter_mysql.sh  && chmod +x exporter_mysql.sh
##./exporter_mysql.sh  --exporters_list=exporter_mysql --exporter_action=deploy --exporter_scrape_allow_net=10.0.0.0/8,95.217.14.7/32,85.15.175.5/32,10.47.0.0/24,127.0.0.1/32 --exporter_mysql_port=9104 --mysql_user=mysqlexporter --mysql_password=57ec2c4a81d5f817 --mysql_host=10.0.2.11 --mysql_port=3306 --pxc_hostname=cluster-2  --docker_placement_exporter_mysql=percona_cluster_node_cluster-2 
for i in "$@"
do
case $i in
    --exporters_list=*) # exporters_list=exporter_mysql
    ARG_EXPORTERS_LIST="${i#*=}"
    shift # past argument=value
    ;;	
    --exporter_action=*) # exporter_action=deploy|remove
    ARG_EXPORTERS_ACTION="${i#*=}"
    shift # past argument=value
    ;;	
    --exporter_scrape_allow_net=*) # exporter_scrape_allow_net="10.0.0.0/8,192.168.0./24"
    ARG_EXPORTERS_SCRAPE_ALLOW_NET="${i#*=}"
    shift # past argument=value
    ;;
    --exporter_mysql_port=*) # exporter_mysql_port=9104
    ARG_EXPORTER_MYSQL_PORT="${i#*=}"
    shift # past argument=value
    ;;	
    --mysql_user=*) # exporter_mysql_user=mysqlexporter
    ARG_MYSQL_USER="${i#*=}"
    shift # past argument=value
    ;;	
    --mysql_password=*) # mysql_password=mysqlexporterpassword
    ARG_MYSQL_PASSWORD="${i#*=}"
    shift # past argument=value
    ;;	
    --mysql_host=*) # exporter_mysql_host=pxc
    ARG_MYSQL_HOST="${i#*=}"
    shift # past argument=value
    ;;
    --mysql_port=*) # exporter_mysql_host=pxc
    ARG_MYSQL_PORT="${i#*=}"
    shift # past argument=value
    ;;
    --pxc_hostname=*) # exporter_mysql_host=pxc
    ARG_PXC_HOSTNAME="${i#*=}"
    shift # past argument=value
    ;;
    --docker_placement_exporter_mysql=*) # percona_cluster_node_${ARG_PXC_HOSTNAME}
    ARG_DOCKER_PLACEMENT_EXPORTER_MYSQL="${i#*=}"
    shift # past argument=value
    ;;
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done


precheck(){

if [ $(dpkg-query -W -f='${Status}' gettext-base 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install gettext-base -y
fi

}

exporter_run(){
    export exporter_prefix="exporter_mysql"
    mkdir -p /docker-compose/SWARM/exporter_mysql;
    cd /docker-compose/SWARM/exporter_mysql;
    curl -q https://gitlab.com/itc-life/public-scripts/raw/master/docker-compose/exporter_mysql/docker-compose.yml.template -o docker-compose.yml.template;
    export docker_placement_exporter_mysql="${ARG_DOCKER_PLACEMENT_EXPORTER_MYSQL}"
    export exporter_mysql_port="${ARG_EXPORTER_MYSQL_PORT}"
    export EXPORTERS_SCRAPE_ALLOW_NET="${ARG_EXPORTERS_SCRAPE_ALLOW_NET}"
    export MYSQL_USER="${ARG_MYSQL_USER}"
    export MYSQL_PASSWORD="${ARG_MYSQL_PASSWORD}"
    export MYSQL_HOST="${ARG_MYSQL_HOST}"
    export MYSQL_PORT="${ARG_MYSQL_PORT}"
    export DOCKER_IMAGE_PROXY_EXPORTER_MYSQL="devsadds/nginx-exporter-proxy-mysql:latest"
    export DOCKER_IMAGE_EXPORTER_MYSQL="prom/mysqld-exporter"
    envsubst < "docker-compose.yml.template" > "docker-compose.yml.percona_cluster_node_${ARG_PXC_HOSTNAME}"
    docker stack rm ${exporter_prefix}_${ARG_PXC_HOSTNAME};
    sleep 20;
    docker stack deploy -c "docker-compose.yml.percona_cluster_node_${ARG_PXC_HOSTNAME}" ${exporter_prefix}_${ARG_PXC_HOSTNAME}

}

main(){
	precheck
	exporter_run
	
}

main