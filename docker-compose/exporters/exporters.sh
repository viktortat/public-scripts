#!/bin/bash

for i in "$@"
do
case $i in
    --exporters_list=*) # exporters_list=exporter_node,cadvisor
    ARG_EXPORTERS_LIST="${i#*=}"
    shift # past argument=value
    ;;	
    --exporter_action=*) # exporter_action=deploy|remove
    ARG_EXPORTERS_ACTION="${i#*=}"
    shift # past argument=value
    ;;	
    --exporter_scrape_allow_net=*) # exporter_scrape_allow_net="10.0.0.0/8,192.168.0./24"
    ARG_EXPORTERS_SCRAPE_ALLOW_NET="${i#*=}"
    shift # past argument=value
    ;;
    --exporter_node_port=*) # exporter_node_port=9100
    ARG_EXPORTER_NODE_PORT="${i#*=}"
    shift # past argument=value
    ;;	
    --cadvisor_port=*) # cadvisor_port=9325
    ARG_CADVISOR_PORT="${i#*=}"
    shift # past argument=value
    ;;		
    --default)
    DEFAULT=YES
    shift # past argument with no value
    ;;
    *)
          # unknown option
    ;;
esac
done



precheck(){
if [ $(dpkg-query -W -f='${Status}' gettext-base 2>/dev/null | grep -c "ok installed") -eq 0 ];
then
  apt-get install gettext-base -y
fi
}
exporter_run(){
	if [[ "${ARG_EXPORTERS_ACTION}" == "deploy" ]];then

		mkdir -p /tmp/etc/prometheus/exporters;
		cd /tmp/etc/prometheus/exporters;
		curl https://gitlab.com/itc-life/public-scripts/raw/master/docker-compose/exporters/docker-compose.yml.template -o docker-compose.yml.template
		export EXPORTERS_SCRAPE_ALLOW_NET=$(echo "${ARG_EXPORTERS_SCRAPE_ALLOW_NET}") \
		&& export exporter_node_port=$(echo "${ARG_EXPORTER_NODE_PORT}") \
		&& export cadvisor_port=$(echo "${ARG_CADVISOR_PORT}") \
		&& envsubst < "docker-compose.yml.template" > "docker-compose.yml"

		if [[ ! -f "/usr/local/bin/docker-compose" ]];then
			curl -L "https://github.com/docker/compose/releases/download/1.25.0/docker-compose-$(uname -s)-$(uname -m)" -o /usr/local/bin/docker-compose
			chmod +x /usr/local/bin/docker-compose
		fi
	
		if [[ ! -d "/etc/prometheus/exporters" ]];then
			mkdir -p /etc/prometheus/exporters
		fi
	
		cd /etc/prometheus/exporters
		mv /tmp/etc/prometheus/exporters/docker-compose.yml .
		/usr/local/bin/docker-compose pull;
		/usr/local/bin/docker-compose up -d;
	fi

	if [[ "${ARG_EXPORTERS_ACTION}" == "remove" ]];then
		for exporter_prefix in $(echo "${ARG_EXPORTERS_LIST}"  | sed "s/,/ /g" )
		do
			docker rm -f exporters_${exporter_prefix}_1 || echo "docker exporters_${exporter_prefix}_1 not exist"
		done

	fi


}

main(){
	precheck
	exporter_run
	
}

main